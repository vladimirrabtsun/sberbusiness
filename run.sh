#!/bin/bash

# Run Selenium Grid with Chrome, Opera and Firefox
docker-compose up -d chrome \
                     opera \
                     firefox \
                     selenium-hub

# Set tests variables
export TEST_TARGET=https://digital.sberbank.kz
export TEST_USER=user123
export TEST_PASSWORD=password123

# Run tests in Chrome
CONFIGURATION_BROWSER=chrome \
docker-compose up maven

## Run tests in Opera
#CONFIGURATION_BROWSER=opera \
#docker-compose up maven
#
## Run tests in Firefox
#CONFIGURATION_BROWSER=firefox \
#docker-compose up maven