package kz.sberbank.sberbusiness.e2e;

import com.codeborne.selenide.Configuration;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class LoginPageTest {

    public static String target = System.getenv("TEST_TARGET");
    public static String user = System.getenv("TEST_USER");
    public static String password = System.getenv("TEST_PASSWORD");

    @BeforeClass
    public static void setupBrowser() {
        Configuration.remote = System.getenv("CONFIGURATION_REMOTE");
        Configuration.browser = System.getenv("CONFIGURATION_BROWSER");
    }

    @Before
    public void openHref() {
        open(target);
    }

    @Test
    public void shouldSeeElements() {
        $(".bank-auth-logo").should(visible);
        $(".select-custom.languagePicker").should(visible);
        $("#log-name").should(visible);
        $("#log-pass").should(visible);
        $("#auth-login").should(visible);
    }

    @Test
    public void shouldLogin() {
        $("#log-name").setValue(user);
        $("#log-pass").setValue(password);
//        $("#auth-login").click();
    }
}
